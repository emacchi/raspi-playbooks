raspi-playbooks
===============

Ansible playbooks that manage a Raspberry Pi installed with
[debian-pi](https://github.com/debian-pi/raspbian-ua-netinst).


## Getting Started

### Prerequisites

* [ansible](http://docs.ansible.com/ansible/intro_installation.html) should be installed.
* Your Raspberry Pi must have been installed with
  [debian-pi](https://github.com/debian-pi/raspbian-ua-netinst).
* You need network access to your Raspberry Pi.
* Your SSH public key should be installed in /root/.ssh/authorized key of your
  Raspberry Pi.

### Configure your environment

The default variables are in group_vars/all. You need to change them and adjust
your configuration.
You'll also need to change the `hosts` file to match with your Raspberry Pi
hostname or FQDN.

### Run the playbooks

```shell
ansible-playbook site.yaml -i hosts
```

### Connect to your Raspberry Pi
```shell
ssh raspi@raspi
```
